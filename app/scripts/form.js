const cyrillicPattern = /^[\u0400-\u04FF]+$/;
ValidSurname = false;
ValidName = false;
ValidPatronym = false;
ValidPhone = false;
ValidApplication = false;
ValidMail = false;
ButtonHandle();

function NoError(attr, attrErrorMessage)
{
    attr.classList.remove("error");
    attrErrorMessage.textContent = "";
    return true
}

function Error(attr, attrErrorMessage)
{
    attr.classList.add("error");
    attrErrorMessage.textContent = "Неверно указаны данные";
    attrErrorMessage.style.color = "red"; // Добавляем красный цвет тексту ошибки
    return false
}

const InputForm = document.getElementById('myForm');
const inputs = [...InputForm.querySelectorAll('input')];
//const selects = [...InputForm.querySelectorAll('select')];
let currentActiveIndex = 0;

function UpDown(tag) {
    tag[currentActiveIndex].focus();

    tag.forEach((input, index) => input.addEventListener('click', () => currentActiveIndex = index))

    InputForm.addEventListener('keydown', (e) => {
        let direction = 0;
    
        switch(e.code) {
            case 'ArrowDown':
            direction = 1;
            break;
            case 'ArrowUp':
            direction = -1;
            break;
            default:
            return;
        }
    
        currentActiveIndex = (tag.length + currentActiveIndex + direction) % tag.length;
    
        tag[currentActiveIndex].focus();
    });
}

UpDown(inputs);
//UpDown(selects);

function CheckStrInput(attr, attrErrorMessage)
{
    for (let letter of attr.value) 
    {
        if (!(letter === '-' || cyrillicPattern.test(letter))) 
        {
            return Error(attr, attrErrorMessage)
        }
    }
    return NoError(attr, attrErrorMessage)
}

//validate surname
document.getElementById("surname").addEventListener("change", surnameHandle);

function surnameHandle(){
    let surname = document.getElementById("surname");
    let surnameErrorMessage = document.getElementById("surname-error-message");
    ValidSurname = CheckStrInput(surname, surnameErrorMessage);
    ButtonHandle();
}

//validate name
document.getElementById("name").addEventListener("change", nameHandle);

function nameHandle(){
    let name = document.getElementById("name");
    let nameErrorMessage = document.getElementById("name-error-message");
    ValidName = CheckStrInput(name, nameErrorMessage);
    ButtonHandle();
}

//validate patronym
document.getElementById("patronym").addEventListener("change", patronymHandle);

function patronymHandle(){
    let patronym = document.getElementById("patronym");
    let patronymErrorMessage = document.getElementById("patronym-error-message");
    ValidPatronym = CheckStrInput(patronym, patronymErrorMessage);
    ButtonHandle();
}


// validate phone_number
document.getElementById("phone_number").addEventListener("change", phoneHandle);

function phoneHandle(){
    let phone = document.getElementById("phone_number");
    let phoneErrorMessage = document.getElementById("phone-error-message");
    let phoneRegex = /^((\+7|8)+([0-9]){10})$/;
    const isValid = phoneRegex.test(document.getElementById("phone_number").value);
    if (isValid) 
    {
        ValidPhone = NoError(phone, phoneErrorMessage);
    } 
    else 
    {
        ValidPhone = Error(phone, phoneErrorMessage);
    }
    ButtonHandle();
}

//validate application_number
document.getElementById("application_number").addEventListener("change", applicationHandle);

function applicationHandle(){
    let application_number = document.getElementById("application_number");
    let applicationErrorMessage = document.getElementById("application-error-message");

    function is_numeric(str)
    {
    if (typeof application_number === 'number')
    {
        return true
    }
    else
    {
        return /^\d+$/.test(str);
    }
    }

    if (is_numeric(application_number.value)) 
    {
        ValidApplication = NoError(application_number, applicationErrorMessage);
    }
    else
    {
        ValidApplication = Error(application_number, applicationErrorMessage);
    }
    ButtonHandle();
}

// validate email_address
document.getElementById("personal_mail").addEventListener("change", mailHandle);

function mailHandle(){
    let mail = document.getElementById("personal_mail");
    let mailErrorMessage = document.getElementById("mail-error-message");
    let mailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const isValid = mailRegex.test(document.getElementById("personal_mail").value);
    if (isValid) 
    {
        ValidMail = NoError(mail, mailErrorMessage);
    } 
    else 
    {
        ValidMail = Error(mail, mailErrorMessage);
    }
    ButtonHandle();
}

document.getElementById("ClickButton").addEventListener("change", ButtonHandle);

function ButtonHandle()
{
    if (ValidSurname && ValidName && ValidPatronym && ValidPhone && ValidApplication && ValidMail)
    {
        document.getElementById("ClickButton").disabled = false;
    }
    else
    {
        document.getElementById("ClickButton").disabled = true
    } 
    // console.log(ValidSurname + " " + ValidName + " " + ValidPatronym + " " + ValidPosition + " " + ValidDepartment + " " + ValidOrganization + " " + ValidPhone + " " + ValidApplication)
}

const body = document.querySelector('body'); // нужно для блокировки скролла внутри body
lockPadding = document.querySelector(".lock-padding");

let unlock = true; // предотвращает двойные нажатия

const timeout = 800; // миллисекунд

const popupLink = document.querySelector('.popup-link');
popupLink.addEventListener("click", async function(e) {
    const popupName = popupLink.getAttribute('href').replace('#', ''); // получаем чистое имя попапа с ссылки
    const currentPopup = document.getElementById(popupName); // получаем сам объект попапа
    document.getElementById("jsonResult").innerHTML = `
Пользователь создается
Подождите пожалуйста!
    `
    popupOpen(currentPopup);
    const surname = document.getElementById("surname").value;
    const name = document.getElementById("name").value;
    const patronym = document.getElementById("patronym").value;
    const position = document.getElementById("position").value;
    const organisation = document.getElementById("organisation").value;
    const phone_number = document.getElementById("phone_number").value;
    const application_number = document.getElementById("application_number").value;
    const lead = document.getElementById("inputData").value;
    const personal_mail = document.getElementById("personal_mail").value;
    const city = document.getElementById("select_city").value;
    const department = document.getElementById("select_department").value;
    const CFO = document.getElementById("select_CFO").value;
    const tuz = document.getElementById("select_tuz").value;
    const response = await fetch('/form', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ "surname": surname, "name": name, "patronym": patronym, "position": position,
        "organisation": organisation, "phone_number": phone_number, "application_number": application_number, 
        "lead": lead, "personal_mail": personal_mail, "city": city, "department": department, "CFO": CFO, "tuz": tuz })
    });
    const result = await response.json();
    if (result.error) {
        document.querySelector('.popup__text').innerHTML = result.error.message;
    }
    else {
        document.getElementById('jsonResult').innerHTML = `
    Фамилия: ${result["surname"]}
    Имя: ${result["name"]}
    Отчество: ${result["patronym"]}
    Должность: ${result["position"]}
    Организация: ${result["organisation"]}
    Номер телефона: ${result["phone_number"]}
    Номер заявки: ${result["application_number"]}
    Руководитель: ${result["lead"]}
    Личная почта: ${result["personal_mail"]}
    Город: ${result["city"]}
    Отдел: ${result["department"]}
    ЦФО: ${result["CFO"]}
    Тип учетной записи: ${result["user_type"]}
       `
    }
    popupOpen(currentPopup); // открытие попапа
    e.preventDefault(); // запрещаем перезагружать страницу, блокируем дальнейшую работу ссылки 
});


const popupCloseIcon = document.querySelectorAll('.close-popup');
if (popupCloseIcon.length > 0) {
    for (let index = 0; index < popupCloseIcon.length; index++) {
        const el = popupCloseIcon[index];
        el.addEventListener("click", function (e) {
            popupClose(el.closest('.popup')); // бежим вверх и ищем родителей с popup, и толкаем их в функцию закрытия, это нужно для того, чтобы можно было закрыть попап нажатием не тольео на крестик, но и на любую темную область экрана
            e.preventDefault();
        });
    }
}

function popupOpen(currentPopup) {
    if (currentPopup && unlock) {
        const popupActive = document.querySelector('.popup.open');
        if (popupActive) {
            popupClose(popupActive, false);
        } else {
            bodyLock();
        }
        currentPopup.classList.add('open');
        currentPopup.addEventListener("click", function (e) {
            if (!e.target.closest('.popup__content')) {
                popupClose(e.target.closest('.popup'));
            }
        });
    }
}

function popupClose(popupActive, doUnlock = true) {
    if (unlock) {
        popupActive.classList.remove('open');
        if (doUnlock) {
            bodyUnlock();
        }
    }
}

function bodyLock() {
    const lockPaddingValue = window.innerWidth - document.querySelector('.wrapper').offsetWidth + 'px';

    const el = lockPadding;
    el.computedStyleMap.paddingRight = lockPaddingValue;
    body.style.paddingRight = lockPaddingValue;
    body.classList.add('lock');

    unlock = false;
    setTimeout(function () {
        unlock = true;
    }, timeout);
}

function bodyUnlock() {
    setTimeout(function () {
        const el = lockPadding;
        el.style.paddingRight = '0px';
        body.style.paddingRight = '0px';
        body.classList.remove('lock');
    }, timeout);

    unlock = false;
    setTimeout(function () {
        unlock = true;
    }, timeout);
}

document.addEventListener('keydown', function (e) {
    if (e.which === 27) {
        const popupActive = document.querySelector('.popup.open');
        popupClose(popupActive);
    }
});

async function get_lead() {

    // образуем select вставив в него данные, взятые из AD
    const inputData = document.getElementById("inputData").value;
    const response = await fetch("/check_name?data=" + inputData);
    const results = await response.json();
    const select = document.getElementById("resultSelect");
    select.innerHTML = "";
    const option = document.createElement("option");
    option.value = 0;
    option.text = "Выберите Своего Руководителя";
    select.appendChild(option);
    
    for (index = 0; index < results[0].length; index++) {
        const option = document.createElement("option");
        option.value = index + 1;
        option.text = results[0][index];
        select.appendChild(option);
    }
    select.style.display = "block";
    
    // вставляем в поле ввода, то, что была выбрано из select
    document.getElementById("resultSelect").addEventListener("change", () => {
        const selectedIndex = document.getElementById("resultSelect").value;
        const select = document.getElementById("resultSelect");
        const inputData = document.getElementById("inputData");
        if (selectedIndex >= 0) {
            inputData.value = select.options[selectedIndex].text;
            select.style.display = "none";
        }
    });
}

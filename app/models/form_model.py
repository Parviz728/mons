from fastapi import Form
from dataclasses import dataclass
from phonenumbers import number_type, parse, carrier

from app.exceptions import InvalidPhoneNumber


# Модель данных для формы ввода.
@dataclass
class FormData:
    surname: str = Form(min_length=2, regex=r'^[А-Яа-яЁё\s-]*$')  # фамилия
    name: str = Form(min_length=2, regex=r'^[А-Яа-яЁё\s-]*$')  # имя
    patronym: str = Form(min_length=2, regex=r'^[А-Яа-яЁё\s-]*$')  # отчество
    position: str = Form(min_length=2)  # должность
    organisation: str = Form(min_length=2)  # организация
    phone_number: str = Form(
        min_length=11,
        max_length=15,
        regex=r'^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$'
    )  # номер телефона
    application_number: int = Form(number_type)  # номер заявки
    lead: str = Form(...) # руководитель
    personal_mail: str = Form(regex=r'^[^\s@]+@[^\s@]+\.[^\s@]+$') # личная почта
    city: str = Form(min_length=2)  # город
    department: str = Form(min_length=2)  # отдел
    CFO: str = Form(...) # ЦФО
    tuz: str = Form(...)  # тип учетной записи (ТУЗ)

    # Производим валидацию введенного номера телефона.
    def __post_init__(self):
        try:
            parsed_number = parse(self.phone_number, region="RU")
            if not carrier._is_mobile(number_type(parsed_number)):
                raise InvalidPhoneNumber(status_code=400, detail="Incorrect phone number")
        except Exception:
            raise InvalidPhoneNumber(status_code=400, detail="Invalid phone number format")

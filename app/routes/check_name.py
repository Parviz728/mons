from typing import Any
from fastapi import APIRouter

from app.services.ad_service import ADService
from app.services.optional_services import OptionalServices


router = APIRouter()


@router.get("/check_name")
async def check_name(data: str) -> tuple[list[Any], list[Any]]:
    """
    Эндпоинт для проверки имени в Active Directory.

    :param data: Имя или фамилия пользователя для поиска.
    :type data: str
    :return: Кортеж из двух списков: полных имен пользователей и их путей в AD.
    :rtype: tuple[list, list]
    """
    ad = ADService()
    op = OptionalServices()
    data = op.transliterate(data)
    all_cn, all_dn = ad.get_lead(data)
    return all_cn, all_dn

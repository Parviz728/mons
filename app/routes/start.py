from fastapi import APIRouter, Request
from fastapi.responses import HTMLResponse

from app.dependencies import templates


router = APIRouter()


@router.get("/", response_class=HTMLResponse)
def show_button(request: Request):
    """
    Эндпоинт для отображения кнопки на HTML-странице.

    :param request: Запрос FastAPI.
    :type request: Request
    :return: HTML-страница с кнопкой.
    :rtype: fastapi.responses.HTMLResponse
    """
    return templates.TemplateResponse('Sign_in.html', {'request': request})

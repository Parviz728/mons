from fastapi import APIRouter, Depends, Request
from fastapi.responses import HTMLResponse

from app.dependencies import (templates, ExchangeSeverInterface,
                              ActiveDirectoryInterface, KeePassInterface, UserProfileInterface)
from app.models.form_model import FormData
from app.services.ad_service import ADService
from app.services.es_service import ESService
from app.services.user_profile import UserProfile
from app.services.optional_services import OptionalServices
from app.services.kee_pass import KeePass


router = APIRouter()


def get_leads_dn(cn):
    ad_service = ADService() 
    return ad_service.get_lead(cn)[1][0]


def get_correct_phone_number(phone_number: str) -> str:
    """
    Функция для получения корректного номера телефона в формате +79998887766.

    :param phone_number: Номер телефона для коррекции.
    :type phone_number: str
    :return: Корректный номер телефона.
    :rtype: str
    """
    correct_phone_number = ""
    for letter in phone_number:
        if letter == "+" or letter.isdigit():
            correct_phone_number += letter
    return correct_phone_number


def login_checker(
        user_profile: UserProfileInterface,
        es_service: ExchangeSeverInterface,
        ad_service: ActiveDirectoryInterface,
        keepass_1c: KeePassInterface
) -> bool:
    """
    Функция для формирования и проверки логина пользователя.

    :param user_profile: Профиль пользователя для создания.
    :type user_profile: UserProfileInterface
    :param es_service: Сервис Exchange Server.
    :type es_service: ExchangeSeverInterface
    :param ad_service: Сервис Active Directory.
    :type ad_service: ActiveDirectoryInterface
    :param keepass_1c: Сервис KeePass.
    :type keepass_1c: KeePassInterface
    :return: True, если логин подходит, иначе False.
    :rtype: bool
    """
    dn = user_profile.dn  # type: ignore[attr-defined]
    # Пока логин не прошел все 3 проверки - добавляем к нему доп. символы.
    while (
            ad_service.is_login_exist(user_profile.login, dn)  # type: ignore[attr-defined]
            or es_service.is_login_exist(user_profile.login)  # type: ignore[attr-defined]
            or keepass_1c.is_login_exist(user_profile.login)  # type: ignore[attr-defined]
    ):
        user_profile.increase_login()
    return True


async def create_user(
    user_profile: UserProfileInterface,
    es_service: ExchangeSeverInterface,
    ad_service: ActiveDirectoryInterface,
    keepass_1c: KeePassInterface
    ) -> None:
    """
    Функция для создания пользователя в Active Directory.

    :param user_profile: Данные для создания пользователя.
    :type user_profile: UserProfileInterface
    """
    if login_checker(user_profile, es_service, ad_service, keepass_1c):
        # Если логин подошел - формируем пользователя в AD.
        await ad_service.create_user(user_profile)


@router.get("/form", response_class=HTMLResponse)
def show_form(request: Request):
    """
    Эндпоинт для отображения HTML-формы ввода.

    :param request: Запрос FastAPI.
    :type request: Request
    :return: HTML-страница с формой ввода.
    :rtype: fastapi.responses.HTMLResponse
    """
    return templates.TemplateResponse('form.html', {'request': request})

# form_data: FormData = Depends()
@router.post("/form")
async def get_form_data(request: Request):
    """
    Эндпоинт для получения данных из формы и создания пользователя.

    :param form_data: Данные формы для создания пользователя.
    :type form_data: FormData
    :return: Данные формы.
    :rtype: FormData
    """
    data = await request.json()
    surname = data.get("surname")
    name = data.get("name")
    patronym = data.get("patronym")
    position = data.get("position")
    organisation = data.get("organisation")
    phone_number = data.get("phone_number")
    application_number = data.get("application_number")
    lead = data.get("lead")
    personal_mail = data.get("personal_mail")
    city = data.get("city")
    department = data.get("department")
    CFO = data.get("CFO")
    tuz = data.get("tuz")
    
    phone_number = get_correct_phone_number(phone_number)
    lead = get_leads_dn(lead)
    
    es_service = ESService()
    ad_service = ADService()
    keepass_1c = KeePass()
    optional_services = OptionalServices()
    form_data = FormData(surname, name, patronym, position, organisation, phone_number, application_number, lead, personal_mail, city, department, CFO, tuz)
    user_profile = UserProfile(form_data, optional_services, test_flag=True)
    # await create_user(user_profile, es_service, ad_service, keepass_1c)
    return user_profile

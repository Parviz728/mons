from app.exceptions import CreateLoginError
from app.models.form_model import FormData
from app.dependencies import (DOMAIN, OptionalServicesInterface,
                              UserProfileInterface)


class UserProfile(UserProfileInterface):
    """
    Класс, определяющий сферу ответственности взаимодействия с профилем юзера.

    :param user_data: Данные пользователя из формы.
    :type user_data: FormData
    :param optional_services: Сервисы для выполнения дополнительных операций.
    :type optional_services: OptionalServicesInterface
    :param test_flag: Флаг тестовых запросов (True - тест,False - реальный запрос).
    :type test_flag: bool
    """
    def __init__(
            self,
            user_data: FormData,
            optional_services: OptionalServicesInterface,
            test_flag: bool = False
    ) -> None:
        # Получение транслитерации имени и фамилии и полного имени
        self.name = optional_services.transliterate(user_data.name)
        self.lead = optional_services.transliterate(user_data.lead)
        self.surname = optional_services.transliterate(user_data.surname)
        self.patronym = optional_services.transliterate(user_data.patronym)
        self.cn = f'{self.surname} {self.name}'  # Полное имя (Common name)

        # Создание сырого логина пользователя
        self.login = self.name[0] + self.surname

        # Генерация пароля пользователя
        self.password = optional_services.generate_ad_password()

        # Тип учетной записи пользователя
        self.user_type = user_data.tuz # type: ignore[attr-defined]

        # Город, департамент, ЦФО, номер телефона и должность пользователя
        self.city = user_data.city
        self.department = user_data.department
        self.CFO = user_data.CFO
        self.phone_number = user_data.phone_number
        self.position = user_data.position

        # Номер заявки, организация и почта пользователя
        self.application_number = user_data.application_number
        self.organisation = user_data.organisation
        self.personal_mail = user_data.personal_mail

        # Получение Имени, Типа домена и полного домена
        domain = DOMAIN.split('.')
        self.dnm = domain[0]  # Имя домена (domain_name)
        self.dty = domain[1]  # Тип домена (domain_type)
        self.dc = f'DC={self.dnm},DC={self.dty}'  # Полный домен

        # Формируем DN нового пользователя
        if test_flag:
            self.dn = f'CN={self.cn},OU=TEST_OU_DAR,{self.dc}'
        else:
            self.dn = f'CN={self.cn},OU={self.department},OU=Users,OU={self.city},{self.dc}'


    def increase_login(self) -> None:
        """
        Функция для инкрементации логина пользователя.

        В случае, если логин не может быть увеличен (достиг максимальной длины),
        вызывается исключение CreateLoginError.

        :raises: CreateLoginError
        """
        if len(self.login) < len(self.name) + len(self.surname):
            self.login = self.name[:len(self.login) - len(self.surname) + 1] + self.surname
        else:
            raise CreateLoginError(status_code=422, detail='Error when creating login')

import random
import string

from app.dependencies import OptionalServicesInterface


class OptionalServices(OptionalServicesInterface):
    """
    Класс, определяющий сферу ответственности взаимодействия с дополнительными сервисами.
    """
    def __init__(self) -> None:
        # Словарь для транслитерации букв.
        self.__translit_table = {
            'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'YO',
            'Ж': 'ZH', 'З': 'Z', 'И': 'I', 'Й': 'Y', 'К': 'K', 'Л': 'L', 'М': 'M',
            'Н': 'N', 'О': 'O', 'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U',
            'Ф': 'F', 'Х': 'KH', 'Ц': 'TS', 'Ч': 'CH', 'Ш': 'SH', 'Щ': 'SHCH',
            'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'YU', 'Я': 'YA'
        }

        # Словарь для обработки окончаний.
        self.__translit_ending = {
            'ИЙ': 'IY',
            'ЫЙ': 'Y'
        }

        # Добавляем буквы и окончания в нижнем регистре.
        self.__translit_table.update(self.__to_lower(self.__translit_table))
        self.__translit_ending.update(self.__to_lower(self.__translit_ending))

        # Допустимые символы для генерации пароля.
        self.__characters = string.ascii_letters + string.digits + string.punctuation

        # Запрещенные символы, которые не должны входить в пароль.
        self.__forbidden_characters = [
            '"', '\\', '/', '[', ']', ':', ';', '|', '=', ',', '+', '*',
            '?', '<', '>', '@', '`', '.', "'", '~', '^', '(', ')', '{', '}'
        ]

        # Формируем список допустимых символов для пароля.
        self.__allowed_characters = [
            char for char in self.__characters if char not in self.__forbidden_characters]


    @staticmethod
    def __to_lower(dictionary: dict) -> dict:
        """
        Функция для добавления элементов в словарь в нижнем регистре.

        :param dictionary: Исходный словарь.
        :type dictionary: dict
        :return: Словарь, где ключи и значения в нижнем регистре.
        :rtype: dict
        """
        to_low = lambda value: ''.join(map(str.lower, value))
        lower_case_items = {to_low(key): to_low(val) for key, val in dictionary.items()}
        return lower_case_items


    def get_privnote_link(self, data_to_hide: str) -> None:
        """
        Функция для упаковки данных и получения ссылки на privnote.

        :param data_to_hide: Данные для упаковки.
        :type data_to_hide: str
        :return: None
        :rtype: None
        """
        raise NotImplementedError


    def transliterate(self, text: str) -> str:
        """
        Функция для транслитерации входного текста.

        :param text: Текст для транслитерации.
        :type text: str
        :return: Транслитерированный текст.
        :rtype: str
        """
        result = ""
        i = 0

        while i < len(text):
            if i < len(text) - 1:
                two_chars = text[i:i + 2]
                if two_chars in self.__translit_ending:
                    result += self.__translit_ending[two_chars]
                    i += 2
                    continue

            char = text[i]
            if char in self.__translit_table:
                result += self.__translit_table[char]
            else:
                result += char

            i += 1
        return result


    def generate_ad_password(self, length: int = 12) -> str:
        """
        Функция для генерации пароля из случайных разрешенных символов.

        :param length: Длина генерируемого пароля (по умолчанию 12 символов).
        :type length: int
        :return: Сгенерированный пароль.
        :rtype: str
        """
        password = ''.join(
            random.choice(self.__allowed_characters) for _ in range(length)
        )
        return password

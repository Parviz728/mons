from app.dependencies import KeePassInterface


class KeePass(KeePassInterface):
    """
    Класс, определяющий сферу ответственности взаимодействия с KeePass.
    """
    def is_login_exist(self, login: str) -> bool:
        """
        Функция для проверки существования логина в KeePass.

        :param login: Логин для проверки.
        :type login: str
        :return: True, если логин уже существует, иначе False.
        :rtype: bool
        """
        return False

from exchangelib import Account, Credentials  # type: ignore[import-untyped]

from app.dependencies import LOGIN, PASSWORD, DOMAIN, ExchangeSeverInterface


username = f'{LOGIN}@{DOMAIN}'
credentials = Credentials(username=username, password=PASSWORD)


class ESService(ExchangeSeverInterface):
    """
    Класс, определяющий сферу ответственности взаимодействия с сервером Exchange.
    """
    @staticmethod
    def is_login_exist(login: str) -> bool:
        """
        Функция проверяющая, есть ли сгенерированный логин в Exchange или нет.

        :param login: Логин для проверки.
        :type login: str
        :return: True, если логин уже существует, иначе False.
        :rtype: bool
        """
        try:
            # Попытка обращения к аккаунту с почтой primary_smtp_address.
            Account(
                primary_smtp_address=f"{login}@korusconsulting.ru",
                credentials=credentials,
                autodiscover=True
            )
            
            Account(
                primary_smtp_address=f"{login}@korusconsulting.com",
                credentials=credentials,
                autodiscover=True
            )
            
            Account(
                primary_smtp_address=f"{login}@korusconsulting.mail.onmicrosoft.com",
                credentials=credentials,
                autodiscover=True
            )
            return True  # Почта с указанным логином существует.
        except Exception as e:
            return False  # Почта с указанным логином не существует.

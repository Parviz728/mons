from typing import Any
from ldap3 import ALL, Server, Connection, SIMPLE, SUBTREE, MODIFY_ADD

from app.dependencies import (DOMAIN, LOGIN, PASSWORD, UserProfileInterface,
                              ActiveDirectoryInterface)


class ADService(ActiveDirectoryInterface):
    """
    Класс, определяющий сферу ответственности взаимодействия с Active Directory.
    """
    async def create_user(self, user_profile: UserProfileInterface) -> None:
        """
        Функция для создания пользователя в AD.

        :param user_profile: Профиль пользователя для создания.
        :type user_profile: UserProfileInterface
        """
        conn = None
        try:
            conn = self.__get_ldap_connection()
            attributes = self.__build_user_attributes(user_profile)
            conn.add(user_profile.dn, attributes=attributes)   # type: ignore[attr-defined]
            if conn.result["result"] == 0:
                print(f'Пользователь успешно создан')
                self.__add_user_to_groups(conn, user_profile)
                
            else:
                print(f'Ошибка создания пользователя: {conn.result}')
        except ConnectionError as e:
            print(f'Ошибка подключения к домену: {e}')
        finally:
            if conn is not None:
                conn.unbind()

    @staticmethod
    def __add_user_to_groups(conn, user_profile):
        """
        Добавляет пользователя в группы безопасности.

        :param conn: Объект подключения к серверу LDAP.
        :type conn: ldap3.Connection
        :param user_profile: Профиль пользователя, который будет добавлен в группы.
        :type user_profile: UserProfileInterface
        """
        # Группы для добавления пользователя в них
        groups = [
            'CN=SG_TEST_OU_DAR,OU=TEST_OU_DAR,DC=korusconsulting,DC=local',
            'CN=SG_TEST_OU_DAR2,OU=TEST_OU_DAR2,OU=Security Groups,OU=SPB_office,'
            'DC=korusconsulting,DC=local'
        ]
        # Словарь изменений для добавления пользователя в группы
        change = {
            'member': [(MODIFY_ADD, [user_profile.dn])]
        }

        for group in groups:
            conn.modify(group, change)


    def get_lead(self, name: str) -> tuple[list[Any], list[Any]]:
        """
        Функция принимает имя/фамилию пользователя и ищет похожих пользователей.

        :param name: Имя или фамилия пользователя.
        :type name: str
        :return: Кортеж из двух списков: полных имен пользователей и их путей в AD.
        :rtype: tuple[list, list]
        """
        conn = None
        try:
            conn = self.__get_ldap_connection()
            search_filter = f"(|(sn=*{name}*)(givenName=*{name}*)(cn=*{name}*))"
            path_to_search = 'DC=korusconsulting,DC=local'
            conn.search(path_to_search, search_filter, SUBTREE)
            list_of_dn = [entry.entry_dn for entry in conn.entries]
            list_of_cn = [data.split(',')[0][3:] for data in list_of_dn]
            return list_of_cn, list_of_dn
        except ConnectionError as e:
            print(f'Ошибка подключения к домену: {e}')
            return [], []
        finally:
            if conn is not None:
                conn.unbind()


    def is_login_exist(self, login: str, dn: str) -> bool:
        """
        Функция проверяет, существует ли логин в AD.

        :param login: Логин пользователя.
        :type login: str
        :param dn: Distinguished Name (DN) для поиска.
        :type dn: str
        :return: True, если логин уже существует, иначе False.
        :rtype: bool
        """
        conn = None
        try:
            conn = self.__get_ldap_connection()
            search_filter = f'(sAMAccountName={login})'
            conn.search(dn, search_filter, SUBTREE)
            return conn.entries != []
        except ConnectionError as e:
            print(f'Ошибка подключения к домену: {e}')
        finally:
            if conn is not None:
                conn.unbind()
        return False


    @staticmethod
    def __build_user_attributes(user_profile: UserProfileInterface) -> dict:
        """
        Функция создает атрибуты пользователя для создания в AD.

        :param user_profile: Профиль пользователя.
        :type user_profile: UserProfileInterface
        :return: Словарь атрибутов пользователя.
        :rtype: dict
        """
        return {
            'objectClass': [
                'top', 'person', 'organizationalPerson', 'user'],  # type: ignore[attr-defined]
            'givenName': user_profile.name,  # type: ignore[attr-defined]
            'sn': user_profile.surname,  # type: ignore[attr-defined]
            'cn': user_profile.cn,  # type: ignore[attr-defined]
            'displayName': user_profile.cn,  # type: ignore[attr-defined]
            'userPrincipalName': (
                f'{user_profile.login}@korusconsulting.ru'),  # type: ignore[attr-defined]
            'mail': (
                f'{user_profile.login}@korusconsulting.ru'),  # type: ignore[attr-defined]
            'sAMAccountName': user_profile.login,  # type: ignore[attr-defined]
            'userPassword': user_profile.password,  # type: ignore[attr-defined]
            'telephoneNumber': user_profile.phone_number,  # type: ignore[attr-defined]
            'Division': "KORUS Consulting",  # type: ignore[attr-defined]
            'title': user_profile.position,  # type: ignore[attr-defined]
            'department': user_profile.department,  # type: ignore[attr-defined]
            'company': user_profile.organisation,  # type: ignore[attr-defined]
            'manager': user_profile.lead,  # type: ignore[attr-defined]
            'extensionAttribute14': user_profile.CFO,  # type: ignore[attr-defined]
            'extensionAttribute15': user_profile.user_type,  # type: ignore[attr-defined]
            'Description': user_profile.application_number,  # type: ignore[attr-defined]
            'mobile': user_profile.phone_number,  # type: ignore[attr-defined]
            'postOfficeBox': user_profile.personal_mail,  # type: ignore[attr-defined]
        }


    @staticmethod
    def __get_ldap_connection():
        """
        Внутренняя функция для получения подключения к LDAP.

        :return: Соединение с LDAP-сервером.
        """
        server = Server(f'ldap://{DOMAIN}', get_info=ALL)
        conn = Connection(server, user=LOGIN, password=PASSWORD, authentication=SIMPLE)
        if not conn.bind():
            raise ConnectionError(f'Ошибка подключения к домену: {conn.result}')
        return conn

from fastapi import HTTPException


class MainException(HTTPException):
    """
    Базовое исключение для наследования.

    :param detail: Описание исключения.
    :type detail: str
    :param status_code: Код статуса HTTP, связанный с исключением (изначально 400).
    :type status_code: int
    """
    def __init__(self, detail: str, status_code: int = 400):
        super().__init__(status_code=status_code, detail=detail)


class InvalidPhoneNumber(MainException):
    """
    Исключение для некорректного номера телефона.
    """
    pass


class CreateLoginError(MainException):
    """
    Исключение для некорректного логина пользователя.
    """
    pass

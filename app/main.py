from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles
from app.exceptions import CreateLoginError, InvalidPhoneNumber

from app.routes import start, form, check_name


app = FastAPI()

# Монтируем статические файлы для стилей и скриптов.
app.mount("/static", StaticFiles(directory="app/static"), name="static")
app.mount("/scripts", StaticFiles(directory="app/scripts"), name="scripts")

# Подключаем API из модулей маршрутов.
app.include_router(start.router)
app.include_router(form.router)
app.include_router(check_name.router)


@app.exception_handler(InvalidPhoneNumber)
async def correct_form(request: Request, exc: InvalidPhoneNumber):
    """
    Обработчик исключения при вводе неверного номера телефона через форму.

    :param request: Запрос FastAPI.
    :type request: Request
    :param exc: Исключение InvalidPhoneNumber.
    :type exc: InvalidPhoneNumber
    :return: JSONResponse с ошибкой и сообщением для пользователя.
    :rtype: JSONResponse
    """
    return JSONResponse(
        status_code=400,
        content={"error": exc.detail,
                 "message": "пожалуйста нажмите на <- и введите корректный номер телефона"
                 }
    )


@app.exception_handler(CreateLoginError)
async def send_error_email(request: Request, exc: CreateLoginError):
    """
    Обработчик исключения при занятости всех возможных логинов.

    :param request: Запрос FastAPI.
    :type request: Request
    :param exc: Исключение CreateLoginError.
    :type exc: CreateLoginError
    """
    raise NotImplementedError

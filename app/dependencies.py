from typing import Any
from decouple import config  # type: ignore[import-untyped]
from abc import ABC, abstractmethod
from fastapi.templating import Jinja2Templates


# Определяем пароль, логин и домен для подключения к нему.
PASSWORD: str = config('PASSWORD')
LOGIN: str = config('LOGIN')
DOMAIN: str = config('DOMAIN')

# Определяем "templates" для всех файлов из "routes".
templates = Jinja2Templates(directory='app/templates')


class UserProfileInterface(ABC):
    """
    Класс, определяющий сферу ответственности взаимодействия с профилем юзера.
    """
    @abstractmethod
    def increase_login(self) -> None:
        """
        Функция для инкрементации логина пользователя.

        В случае, если логин не может быть увеличен (достиг максимальной длины),
        вызывается исключение CreateLoginError.

        :raises: CreateLoginError
        """
        pass


class ExchangeSeverInterface(ABC):
    """
    Класс, определяющий сферу ответственности взаимодействия с сервером Exchange.
    """
    @staticmethod
    @abstractmethod
    def is_login_exist(login: str) -> bool:
        """
        Функция проверяющая, есть ли сгенерированный логин в Exchange или нет.

        :param login: Логин для проверки.
        :type login: str
        :return: True, если логин уже существует, иначе False.
        :rtype: bool
        """
        pass


class ActiveDirectoryInterface(ABC):
    """
    Класс, определяющий сферу ответственности взаимодействия с Active Directory.
    """
    @abstractmethod
    async def create_user(
            self,
            user_profile: UserProfileInterface
    ) -> None:
        """
        Функция для создания пользователя в AD.

        :param user_profile: Профиль пользователя для создания.
        :type user_profile: UserProfileInterface
        """
        pass


    @abstractmethod
    def get_lead(self, name: str) -> tuple[list[Any], list[Any]]:
        """
        Функция принимает имя/фамилию пользователя и ищет похожих пользователей.

        :param name: Имя или фамилия пользователя.
        :type name: str
        :return: Кортеж из двух списков: полных имен пользователей и их путей в AD.
        :rtype: tuple[list, list]
        """
        pass


    @abstractmethod
    def is_login_exist(self, login: str, dn: str) -> bool:
        """
        Функция проверяет, существует ли логин в AD.

        :param login: Логин пользователя.
        :type login: str
        :param dn: Distinguished Name (DN) для поиска.
        :type dn: str
        :return: True, если логин уже существует, иначе False.
        :rtype: bool
        """
        pass


class KeePassInterface(ABC):
    """
    Класс, определяющий сферу ответственности взаимодействия с KeePass.
    """
    @abstractmethod
    def is_login_exist(self, login: str) -> bool:
        """
        Функция для проверки существования логина в KeePass.

        :param login: Логин для проверки.
        :type login: str
        :return: True, если логин уже существует, иначе False.
        :rtype: bool
        """
        pass


class OptionalServicesInterface(ABC):
    """
    Класс, определяющий сферу ответственности взаимодействия с дополнительными сервисами.
    """
    @abstractmethod
    def get_privnote_link(self, data_to_hide: str) -> None:
        """
        Функция для упаковки данных и получения ссылки на privnote.

        :param data_to_hide: Данные для упаковки.
        :type data_to_hide: str
        :return: None
        :rtype: None
        """
        pass


    @abstractmethod
    def transliterate(self, text: str) -> str:
        """
        Функция для транслитерации входного текста.

        :param text: Текст для транслитерации.
        :type text: str
        :return: Транслитерированный текст.
        :rtype: str
        """
        pass


    @abstractmethod
    def generate_ad_password(self, length: int = 12) -> str:
        """
        Функция для генерации пароля из случайных разрешенных символов.

        :param length: Длина генерируемого пароля (по умолчанию 12 символов).
        :type length: int
        :return: Сгенерированный пароль.
        :rtype: str
        """
        pass
